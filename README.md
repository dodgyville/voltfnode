Hi, I've made the repo for this project public in time for #pyconau 2019. However it's probably a few weeks too early. Not much documentation yet and not user friendly to set up.

*** It also probably won't run without massaging the dependencies and file structure. ***


# About
This repo contains two apps:

* "voltfnode app" - a kotlin app to install on android phones
* "voltfnode server" - a webapp to run on a computer and control phones running voltfnode app.

app:
Open app in android studio and build for the target phone.

server:
pip3 install flask-humanize flask-restful pyvoltflib voltf2psx Pillow comparephotos

Note: Currently some of the modules are not up on my pypi yet. You also need to modify opencamera app. See my talk for details. Will progressively update this over the next few weeks.

