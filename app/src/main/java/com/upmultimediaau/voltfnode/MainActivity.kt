package com.upmultimediaau.voltfnode

import android.Manifest
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.BroadcastReceiver
import android.database.Cursor
import android.database.MergeCursor
import android.graphics.Color
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraDevice.TEMPLATE_PREVIEW
import android.hardware.camera2.CameraManager
import android.media.MediaRecorder
import android.provider.MediaStore
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import android.media.CamcorderProfile
import android.media.Image
import android.media.ImageReader
import android.net.ConnectivityManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.*
import android.provider.Settings
import android.text.format.Formatter
import android.view.MotionEvent
import android.view.Surface
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import com.github.kittinunf.fuel.core.DataPart
import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.httpGet
import org.joda.time.DateTime
import org.json.JSONObject
import org.ocpsoft.prettytime.PrettyTime
import java.io.*
import java.lang.NumberFormatException
import java.lang.ref.WeakReference
import java.net.*
import java.nio.charset.StandardCharsets
import java.nio.file.Files.createFile
import java.security.AccessController.getContext
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.concurrent.timer
import kotlin.jvm.internal.Ref
import kotlin.reflect.KFunction



const val VERSION = "0.0.17"
const val NEUTRAL = "#CBF0EC"
const val FAIL = "#F0CBDE"
const val STRONG = "#CBF0CF"

val VOLTF_WAITING = 0
val VOLTF_CAMERA_ON = 1
val VOLTF_CAMERA_OFF = 2
val VOLTF_CAMERA_START = 3
val VOLTF_CAMERA_STOP = 3
val VOLTF_CAMERA_IMAGE = 4
val VOLTF_ASK_FOR_NAME = 5  // tell the phone to start asking for a name
val VOLTF_REQUEST_NAME = 6 // request name
val VOLTF_UPLOAD_IMAGE = 7
val VOLTF_UPLOAD_VIDEO = 8
val VOLTF_REREGISTER = 9
val VOLTF_STOPPOLLING = 10
val VOLTF_VIDEOINTENT = 11
val VOLTF_CAMERAINTENT = 12
var VOLTF_VIDEOINTENT_START = 13
var VOLTF_VIDEOINTENT30 = 14
var VOLTF_SELFIE = 15
var VOLTF_VIDEOINTENTDURATION = 16
var VOLTF_UPLOAD_STATIC = 17
// var VOLTF_PREVIEW_START = 18



val VOLTF_STATES = mapOf(
    VOLTF_WAITING to "waiting",
    VOLTF_CAMERA_ON to "camera on",
    VOLTF_CAMERA_OFF to "camera off",
    VOLTF_CAMERA_START to "camera start",
    VOLTF_CAMERA_STOP to "camera stop",
    VOLTF_UPLOAD_IMAGE to "upload image",
    VOLTF_UPLOAD_VIDEO to "upload video",
    VOLTF_REREGISTER to "register node",
    VOLTF_STOPPOLLING to "stop polling",
    VOLTF_VIDEOINTENT to "open video recorder",
    VOLTF_CAMERAINTENT to "open camera app",
    VOLTF_VIDEOINTENT_START to "send stop video",
    VOLTF_VIDEOINTENT30 to "30 second record",
    VOLTF_SELFIE to "take selfie",
    VOLTF_VIDEOINTENTDURATION to "variable record",
    VOLTF_UPLOAD_STATIC to "upload static video"
)


val REQUEST_VIDEO_CAPTURE = 1
val REQUEST_IMAGE_CAPTURE = 2
val REQUEST_VIDEO_STATIC = 3

class Response(json: String) : JSONObject(json) {
    val type: String? = this.optString("type")
    val data = this.optJSONArray("data")
        ?.let { 0.until(it.length()).map { i -> it.optJSONObject(i) } } // returns an array of JSONObject
        ?.map { VoltfResponse(it.toString()) } // transforms each JSONObject of the array into VoltfResponse
}

class VoltfResponse(json: String) : JSONObject(json) {
    val id = this.optInt("id")
    val duration = this.optInt("duration")
    val voltf_status = this.optInt("voltf_status")
}

class NetworkUtil() {
    var TYPE_WIFI = 1;
    var TYPE_MOBILE = 2;
    var TYPE_NOT_CONNECTED = 0;
    var NETWORK_STATUS_NOT_CONNECTED = 0;
    var NETWORK_STATUS_WIFI = 1;
    var NETWORK_STATUS_MOBILE = 2;

    public fun getConnectivityStatus(context: Context) : Int {
        var cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager;

        var activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        } 
        return TYPE_NOT_CONNECTED;
    }

    public fun getConnectivityStatusString(context: Context) : Int {
        var conn = this.getConnectivityStatus(context);
        var status = 0;
        if (conn == this.TYPE_WIFI) {
            status = NETWORK_STATUS_WIFI;
        } else if (conn == this.TYPE_MOBILE) {
            status = NETWORK_STATUS_MOBILE;
        } else if (conn == this.TYPE_NOT_CONNECTED) {
            status = NETWORK_STATUS_NOT_CONNECTED;
        }
        return status;
    }
}


class VoltfNodeRunOnStartup : BroadcastReceiver() {
    override public fun onReceive(context: Context, intent: Intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            var i = Intent(context, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(i)
        }
    }

}


class VoltfNodeRunOnWiFi : BroadcastReceiver() {
    override public fun onReceive(context: Context, intent: Intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            var status = NetworkUtil().getConnectivityStatusString(context);
            if (status != NetworkUtil().NETWORK_STATUS_NOT_CONNECTED) {
                var uplIntent = Intent(context, MainActivity::class.java)
                uplIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(uplIntent);

                var pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager;
                var wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK or PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP), "voltfnode:myWakeLock");
                wakeLock.acquire();

                var keyguardManager:KeyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager;
                var keyguardLock = keyguardManager.newKeyguardLock("voltfnode:myKeyguardLock");
                keyguardLock.disableKeyguard();
            }
        }
    }
}


class MainActivity : AppCompatActivity() {

    private lateinit var textMessage: TextView
    private var server_ip = "192.16.1.1"
    private var server_port = "5000"
    private var camera_id = -1
    private var voltf_status = VOLTF_WAITING
    private var voltf_last_seen : DateTime? = null
    private var pollingTimer = Timer()
    private var polling = false
    private var mediaRecorder: MediaRecorder? = null
    private var surfaceTexture:  SurfaceTexture? = null
    private var surface : Surface? = null
    private var cameraSession : CameraCaptureSession? = null
    private var pollingPaused : Boolean = false
    private var receiveDataCallback: ((String?)->Unit)? = null
    private var cameraView: View? = null
    private var currentPhotoPath: String = ""
    private var currentVideoPath: String = ""
    private var currentDuration: Int = 30
    private var currentStaticPath: String = ""
    private var freeBytesExternal: Long = 0
    private var freeBytesInternal: Long = 0


    fun get_server_url(new_ip: String = ""): String {
        if (new_ip.length > 0) {
            this.server_ip = new_ip
        }
        return "http://" + this.server_ip + ":" + this.server_port + "/"
    }

    /** Helper to ask camera permission.  */
    object CameraPermissionHelper {
        private const val CAMERA_PERMISSION_CODE = 0
        private const val CAMERA_PERMISSION = Manifest.permission.CAMERA

        private const val RECORD_AUDIO_CODE = 0
        private const val RECORD_AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO

        private const val INTERNET_CODE = 0
        private const val INTERNET_PERMISSION = Manifest.permission.INTERNET

        private const val ACCESS_WIFI_STATE_CODE = 0
        private const val ACCESS_WIFI_STATE_PERMISSION = Manifest.permission.ACCESS_WIFI_STATE

        private const val RECEIVE_BOOT_COMPLETED_CODE = 0
        private const val RECEIVE_BOOT_COMPLETED_PERMISSION = Manifest.permission.RECEIVE_BOOT_COMPLETED


        /** Check to see we have the necessary permissions for this app.  */
        fun hasCameraPermission(activity: Activity): Boolean {
            return ContextCompat.checkSelfPermission(activity, CAMERA_PERMISSION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, RECORD_AUDIO_PERMISSION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, INTERNET_PERMISSION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, ACCESS_WIFI_STATE_PERMISSION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, RECEIVE_BOOT_COMPLETED_PERMISSION) == PackageManager.PERMISSION_GRANTED
        }

        /** Check to see we have the necessary permissions for this app, and ask for them if we don't.  */
        fun requestCameraPermission(activity: Activity) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(CAMERA_PERMISSION), CAMERA_PERMISSION_CODE
            )
        }


        /** Check to see we have the necessary permissions for this app, and ask for them if we don't.  */
        fun requestAudioPermission(activity: Activity) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(RECORD_AUDIO_PERMISSION), RECORD_AUDIO_CODE
            )
        }

        fun requestInternetPermission(activity: Activity) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(INTERNET_PERMISSION), INTERNET_CODE
            )
        }

        fun requestAccessWifiStatePermission(activity: Activity) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(ACCESS_WIFI_STATE_PERMISSION), ACCESS_WIFI_STATE_CODE
            )
        }
        fun requestReceiveBootCompletedPermission(activity: Activity) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(RECEIVE_BOOT_COMPLETED_PERMISSION), RECEIVE_BOOT_COMPLETED_CODE
            )
        }


        /** Check to see if we need to show the rationale for this permission.  */
        fun shouldShowRequestPermissionRationale(activity: Activity): Boolean {
            return ActivityCompat.shouldShowRequestPermissionRationale(activity, CAMERA_PERMISSION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(activity, RECORD_AUDIO_PERMISSION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(activity, INTERNET_PERMISSION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(activity, ACCESS_WIFI_STATE_PERMISSION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(activity, RECEIVE_BOOT_COMPLETED_PERMISSION)

        }

        /** Launch Application Setting to grant permission.  */
        fun launchPermissionSettings(activity: Activity) {
            println("launch permission settings")
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            intent.data = Uri.fromParts("package", activity.packageName, null)
            activity.startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(
                this, "Camera permission is needed to run this application", Toast.LENGTH_LONG
            )
                .show()
            if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                CameraPermissionHelper.launchPermissionSettings(this)
            }
            finish()
        }
        recreate()
        println("finish onRequestPermissionsResult")

    }

    private fun hasCamera(): Boolean {
        return packageManager.hasSystemFeature(
            PackageManager.FEATURE_CAMERA_ANY
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        println("*")
        println("*")
        println("*")
        println("* STARTING VOLTFNODE")
        println("*")
        println("*")
        println("*")

        val sdkint =  Build.VERSION.SDK_INT
        if (sdkint > 8) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

        }
        if (!hasCamera()) {
            println("************* NO CAMERA ON DEVICE")
        }

        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            println("voltfnode does not have all permissions")
            CameraPermissionHelper.requestCameraPermission(this)
            CameraPermissionHelper.requestAudioPermission(this)
            CameraPermissionHelper.requestInternetPermission(this)
            CameraPermissionHelper.requestAccessWifiStatePermission(this)
            CameraPermissionHelper.requestReceiveBootCompletedPermission(this)
            return
        }

        btnRegister.setOnClickListener { registerPhone() }

        btnPolling.setOnClickListener { togglePolling() }
        btn30sec.setOnClickListener { dispatchStartVideo30Event() }
        btnSnapshot.setOnClickListener{ dispatchTakePhotoEvent() }
        btnStaticVideo.setOnClickListener { dispatchStartStaticVideo() }

        val editTextDeviceName = findViewById<EditText>(R.id.editTextDeviceName)
        editTextDeviceName.setText(Build.MODEL)

        val textViewVersion = findViewById<TextView>(R.id.textViewVersion)
        val t = "Version $VERSION"
        textViewVersion.text = t

        // set device ID based on network address
        findViewById<EditText>(R.id.editTextID).also {
            it.setText(this.getLocalIpNumber())
        }

        this.updateStats()
        // wait a bit for wifi to connect then search for server
        Timer().schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    scanForServer()
                }
            }
        }, 8000)


        Timer().schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    healthCheck()
                }
            }
        }, 0, 5500) // mention time interval after which your xml will be hit.

    }


    private fun getLocalIpNumber(): String? {
        // returns network device id
        try {

            val wifiManager: WifiManager = getApplicationContext()?.getSystemService(WIFI_SERVICE) as WifiManager
            return ipEndToString(wifiManager.connectionInfo.ipAddress)
        } catch (ex: Exception) {
            println("IP Address"+ ex.toString())
        }
        return null
    }

    private fun getLocalIpHead(): String? {
        // returns network subnet (eg 192.168.1)
        try {

            val wifiManager: WifiManager = getApplicationContext()?.getSystemService(WIFI_SERVICE) as WifiManager
            return ipHeadToString(wifiManager.connectionInfo.ipAddress)
        } catch (ex: Exception) {
            println("IP Address"+ ex.toString())
        }
        return null
    }



    private fun ipToString(i: Int): String {
        return (i and 0xFF).toString() + "." +
                (i shr 8 and 0xFF) + "." +
                (i shr 16 and 0xFF) + "." +
                (i shr 24 and 0xFF)

    }

    private fun ipEndToString(i: Int): String {
        return (i shr 24 and 0xFF).toString()

    }

    private fun ipHeadToString(i: Int): String {
        return (i and 0xFF).toString() + "." +
                (i shr 8 and 0xFF) + "." +
                (i shr 16 and 0xFF) + "."
    }

    private fun checkPort(host: String, port: Int) : Boolean {
        var sockaddr = InetSocketAddress(host, port);
        // Create your socket
        var socket = Socket()
        var online = true
        // Connect with 1s timeout
        try {
            socket.connect(sockaddr, 1000);
        } catch (stex: SocketTimeoutException) {
            // treating timeout errors separately from other io exceptions
            // may make sense
            println("time out")
            online=false
        } catch (iOException: IOException ) {
            println("exception")
            online = false;
        } finally {
            // As the close() operation can also throw an IOException
            // it must caught here
            println("finally")
            try {
                socket.close();
            } catch (ex: IOException) {
                // feel free to do something moderately useful here, eg log the event
            }

        }
        // Now, in your initial version all kinds of exceptions were swallowed by
        // that "catch (Exception e)".  You also need to handle the IOException
        // exec() could throw:
        return online
    }

    private fun scanForServer(): Boolean {
        var found = false
        var deviceid = 1
        println("scanning for server")
        val textViewStatus = findViewById(R.id.textViewStatus) as TextView
        textViewStatus.setText("scanning for server")
        while (!found && deviceid < 120) {
            var r: String = ""
            var testIP = "http://"+this.getLocalIpHead() + deviceid.toString()+":" + server_port
            var url: String = testIP + "/hello/"
            println("testing if server exists at " + url)
            var result = this.sendGet("/hello/", testIP, 2000)
            if (result.length>0) {
                println("Found")
                // set server address
                findViewById<EditText>(R.id.editTextServerAddress).also {
                    it.setText(this.getLocalIpHead() + deviceid.toString())
                }
                this.startPolling();
                return true;
            }
            /*
            try  {
                var found = this.checkPort(url, 5000)
                // set server address
                findViewById<EditText>(R.id.editTextServerAddress).also {
                    it.setText(testIP)
                }
                println("Found")
                return found
            } catch (e: FileNotFoundException) {

                println("did not receive any response from server, skipping for ${this.camera_id}")
            }
            */
            // response will contain version, server app, available features
            /*
            var vresponse : VoltfResponse? = null
            try {
                vresponse = VoltfResponse(r)
            } catch (e: java.lang.Exception) {
                println("did not receive a valid response from server, skipping for ${this.camera_id}")
                return;
            }
            */
            deviceid += 1
        }
        if (!found) {
            textViewStatus.setText("finished scanning for server")
            Toast.makeText(this, "No server found.",  Toast.LENGTH_LONG).show()
        }
        println("Found?"+found)
        return found
    }

    private fun status() : String? {
        if (this.voltf_status in VOLTF_STATES) {
            return VOLTF_STATES[this.voltf_status]
        } else {
            return "unknown"
        }
    }

    private fun healthCheck() {
        // update stats on screen depending on health of app
        val textViewStatus = findViewById(R.id.textViewStatus) as TextView
        println("health check ${this.camera_id} last seen: ${this.voltf_last_seen}")
        if (null == this.voltf_last_seen) {
            textViewStatus.setBackgroundColor(Color.parseColor(NEUTRAL))
            return
        } else if (this.pollingPaused) {
            textViewStatus.setBackgroundColor(Color.parseColor(NEUTRAL))
            textViewStatus.setText("Status: uploading data")
            return
        }
        val t = PrettyTime().format(this.voltf_last_seen?.toDate())
        textViewStatus.setText("Status: ${this.status()} (${t})")
        if (this.voltf_last_seen!! <= DateTime.now().minusSeconds(10)) {
            textViewStatus.setBackgroundColor(Color.parseColor(FAIL))
        } else if (this.voltf_last_seen!! > DateTime.now().minusSeconds(4)) {
            textViewStatus.setBackgroundColor(Color.parseColor(STRONG))
        }
    }



    private fun updateStats() {
        this.freeBytesInternal = File(getApplicationContext().getFilesDir().getAbsoluteFile().toString()).freeSpace
        this.freeBytesExternal = File(getExternalFilesDir(null).toString()).freeSpace
    }

    private fun stats() : String {
        // return stats about this machine in url param form
        return "?internal="+this.freeBytesInternal+"&external="+this.freeBytesExternal+"&version="+VERSION
    }


    private fun sendGet(path: String, server: String = "", timeout: Int = 15000): String {
        var url = server
        if (server.length == 0) {
            url = this.get_server_url()
        }
        var fullURL = url + path + this.stats()
        println("will connect to $fullURL")
        var allText : String = ""

        val (request, response, result) = fullURL
            .httpGet().timeout(timeout)
            .responseString()

        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
                println(ex)
            }
            is Result.Success -> {
                allText = result.get()
                println("return from get "+allText)
            }
        }
        this.voltf_last_seen = DateTime.now()
        this.resumePolling()
        return allText
    }

    private fun sendGetOld(path: String, server: String = ""): String {
        var url = server
        if (server.length == 0) {
            url = this.get_server_url()
        }
        var fullURL = URL(url + path + this.stats())
        println("will connect to $fullURL")
        var allText : String

        val urlConnection = fullURL.openConnection() as HttpURLConnection


        try {
            allText = urlConnection.inputStream.bufferedReader().readText()
            println("return value $allText")
        } finally {
            urlConnection.disconnect()
            this.voltf_last_seen = DateTime.now()
            this.resumePolling()
        }
        return allText
    }

    private fun sendFile(path: String, file: File) {
        val serverURL: String = this.get_server_url() + path
        val url = URL(serverURL)
        println("will connect to $url")
        this.pausePolling()

        var allText : String?  = "-1"
        Fuel.upload(url.toString()).request.add {
             FileDataPart(file)
        }.responseString { _request, response, result ->
             println("RESPONSE FROM SERVER for sendFile $response")
            this.voltf_last_seen = DateTime.now()

            println("try to resume polling from sendFile")
            this.resumePolling()

            val (r, error) = result
            allText = r
            this.receiveDataCallback!!.invoke(allText)
        }
        println("started sendFile ${file.name}")
    }


    private fun sendPost(path: String, formData: List<Pair<String, Any?>>, file: File) {
        """
            dataJson is a string containing data in json format
        """
        val serverURL: String = this.get_server_url() + path
        val url = URL(serverURL)
        this.pausePolling()
        println("will connect to $url")
        var allText : String? = "-1"
        //val formData = listOf("Email" to "mail@example.com", "Name" to "Joe Smith" )
        Fuel.upload(url.toString(), parameters = formData).request.add {
            FileDataPart(file)
        }.responseString { _request, response, result ->
            result.fold(success = {
                println("RESPON SE FROM SERVER for sendPost $response")
                this.voltf_last_seen = DateTime.now()
                val (r, error) = result
                allText = r
                this.receiveDataCallback!!.invoke(allText)
                println("try to resume polling from sendPost")
                this.resumePolling()
            }, failure = {
                println("failure from sendPost $it")
                this.resumePolling()
            })
        }
        println("started sendPost ${file.name} is ${allText}")
    }


    private fun registerPhone() {
        println("Register")
        var editTextServerAddress = findViewById(R.id.editTextServerAddress) as EditText
        var editTextDeviceName = findViewById(R.id.editTextDeviceName) as EditText
        var editTextID = findViewById(R.id.editTextID) as EditText
        var editTextPosition = findViewById(R.id.editTextPosition) as EditText

        val ip = this.get_server_url(editTextServerAddress.text.toString());
        println("Updated server to $ip")

        val name = editTextDeviceName.text.toString()
        val formData = listOf("name" to name,
            "id" to editTextID.text.toString(),
            "position" to editTextPosition.text.toString())

        //this.camera_id = this.sendGet("cameras/register/").trim().toInt()
        var file = File("/storage/self/primary/Movies", "image.jpg")
        if (!file.exists()) {
            println("No votlfnode image, so fall back to camera gallery")
            try {
                file = this.getMostRecentImage()!!
            } catch (e: KotlinNullPointerException) {
                //Toast.makeText(this, "Hi there3! We need at least one photo on the phone.", Toast.LENGTH_LONG).show()
                null
            }
        }
        if (!file.exists()) {
            Toast.makeText(this, "Hi there2! We need at least one photo on the phone.", Toast.LENGTH_LONG).show()
            file = File.createTempFile("temp", ".tmp")
        }
        this.receiveDataCallback = {
            val vresponse = it?.let { it1 -> VoltfResponse(it1) }
            //a -> this.camera_id = a!!.trim().toInt()
            println(vresponse)
            this.camera_id = vresponse!!.id
            this.currentDuration = vresponse!!.duration
        }
        println("starting sendPost for register")
        this.sendPost("cameras/register/", formData, file)
        this.voltf_status = VOLTF_WAITING
    }


    private fun sendShot() {
        // send the current image shot to the server
        println("Upload image")
        // val file = File("/storage/self/primary/Movies", "image.jpg")
        //val file = this.getMostRecentImage()!!
        val file = File(this.currentPhotoPath);
        println("Want to send ${file.absolutePath} which exists? ${file.exists()}")
        this.sendFile("camera/${this.camera_id}/uploadshot/", file)
    }

    private fun sendVideo() {
        // send the current video to the server
        println("Upload video")
        val file = File(this.currentVideoPath);
        println("Want to send video ${file.absolutePath} which exists? ${file.exists()}")
        this.sendFile("camera/${this.camera_id}/uploadvideo/", file)
    }

    private fun sendStatic() {
        // send the current static video to the server
        println("Upload static video")
        val file = File(this.currentStaticPath);
        println("Want to send video ${file.absolutePath} which exists? ${file.exists()}")
        this.sendFile("camera/${this.camera_id}/uploadstatic/", file)
    }

    private fun togglePolling() {
        if (!this.polling) {
            this.startPolling()
        } else {
            this.stopPolling()
        }
    }


    private fun setBtnPolling(txt: String) {
        Timer().schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    println("update btn text2")

                    btnPolling.text = txt
                }
            }
        }, 10)
    }

    private fun pausePolling() {
        this.pollingPaused = true
        println("pause polling ${this.camera_id}")
        btnPolling.text = "Polling Paused"
    }

    private fun resumePolling() {
        println("resume polling")
        if (this.pollingPaused) {
            this.pollingPaused = false
            this.setBtnPolling("Stop Polling")
            println("update btn text")
        }
    }

    private fun stopPolling(label:String = "Start Polling") {
        btnPolling.text = label
        this.polling = false
        this.pollingTimer.cancel()
    }


    private fun startPolling() {
        val connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (!mWifi.isConnected()) {
            Toast.makeText(this, "Hi there! WiFi does not appear to be connected.", Toast.LENGTH_LONG).show()
            return
        }
        btnPolling.text = "Stop Polling"
        println("start polling")
        val main = this
        this.polling = true
        this.pollingTimer = Timer()
        this.pollingTimer.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    try {
                        checkValkyrie()
                    } catch (e: ConnectException) {
                        println("Connection error, will try again soon for ${main.camera_id}")
                    }
                }
            }
        }, 0, 2000) // mention time interval after which your xml will be hit.
    }

    private fun checkValkyrie() {
        if (this.pollingPaused) {
            return
        }
        println("Check Valkryie")
        if (this.camera_id == -1) {
            this.registerPhone()
            return
        }
        println("Polling valkyrie ${this.camera_id}")
        var r: String = ""
        try  {
            r = this.sendGet("camera/" + this.camera_id + "/instruction/")
        } catch (e: FileNotFoundException) {
            println("did not receive any response from server, skipping for ${this.camera_id}")
        }
        var vresponse : VoltfResponse? = null
        try {
            vresponse = VoltfResponse(r)
        } catch (e: java.lang.Exception) {
            println("did not receive a valid response from server, skipping for ${this.camera_id}")
            return;
        }

        val instruction = vresponse.voltf_status
        this.currentDuration = vresponse.duration

        this.voltf_last_seen = DateTime.now()
        if (instruction == VOLTF_REREGISTER) {   // force a reregister
            this.voltf_status = VOLTF_WAITING
        }
        if (instruction != this.voltf_status) {
            this.voltf_status = instruction
            println("NEW INSTRUCTION ${this.voltf_status}")
            if (instruction == VOLTF_VIDEOINTENT_START) {
                this.dispatchStartVideoEvent()
            } else if (instruction == VOLTF_VIDEOINTENT30) {
                this.dispatchStartVideo30Event()
            } else if (instruction == VOLTF_VIDEOINTENTDURATION) {
                println("Starting video with duration ${this.currentDuration}")
                this.dispatchStartVideoDuration(this.currentDuration)
            } else if (instruction == VOLTF_CAMERA_IMAGE) {
                this.dispatchTakePhotoEvent()
            } else if (instruction == VOLTF_SELFIE) {
                this.dispatchTakePhotoEvent(true)
            } else if (instruction == VOLTF_UPLOAD_IMAGE) {
                this.sendShot()
            } else if (instruction == VOLTF_UPLOAD_VIDEO) {
                //var f = this.getMostRecentVideo()
                //this.sendFile("camera/${this.camera_id}/uploadvideo/", f!!)
                this.sendVideo()
            } else if (instruction == VOLTF_UPLOAD_STATIC) {
                //var f = this.getMostRecentVideo()
                this.sendStatic()

            } else if (instruction == VOLTF_STOPPOLLING) {
                this.stopPolling()
            } else if (instruction == VOLTF_REREGISTER) {
                this.registerPhone()
            } else if (instruction == VOLTF_WAITING) {
                println("Entering wait ${this.camera_id}")
            } else {
                println("Don't understand instruction ${instruction}")
            }
        } else {
            // print(" no new instruction")
        }

    }



    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        // return File("/storage/self/primary/Movies", "snapshot.jpg")

        return File.createTempFile(
            "snapshot_${this.camera_id}_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    @Throws(IOException::class)
    private fun createVideoFile(intent_type: Int = REQUEST_VIDEO_CAPTURE): File {
        // Create an video file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_MOVIES)!!
        var prefix = ""
        if (intent_type == REQUEST_VIDEO_STATIC) {
            prefix = "track_${this.camera_id}_${timeStamp}_"
        } else {
            prefix = "static_${this.camera_id}_${timeStamp}_"
        }

        return File.createTempFile(
            prefix, /* prefix */
            ".mp4", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            if (intent_type == REQUEST_VIDEO_CAPTURE) {
                currentVideoPath = absolutePath
            } else {
                currentStaticPath = absolutePath
            }
        }
    }


    private fun dispatchTakePhotoEvent(selfie: Boolean = false){
        // open "open camera" app and pass automation request
        val intent: Intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        this.pausePolling()

        println("start voltf image camera")

        // intent.putExtra("voltfnode.AUTO_OPERATE", 5);
        intent.putExtra("voltfnode.AUTO_SNAPSHOT", true);
        intent.also { takeImageIntent ->
            takeImageIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    println("Unable to create image file")
                    null
                }
                print("Got file ${photoFile?.absoluteFile}");
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.example.android.fileprovider",
                        it
                    )
                    println("passing in photo uri")
                    takeImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    if (selfie) {
                        takeImageIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true)
                    }
                    startActivityForResult(takeImageIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }


    private fun dispatchStartVideoDuration(duration: Int = 0, intent_type: Int = REQUEST_VIDEO_CAPTURE){
        // open "open camera" app and pass automation request
        val intent: Intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)

        println("start voltf video camera")
        this.pausePolling()


        if (duration > 0) {
            intent.putExtra("voltfnode.AUTO_OPERATE", duration);
        }

        intent.also { takeVideoIntent ->
            takeVideoIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val videoFile: File? = try {
                    createVideoFile(intent_type)
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    println("Unable to create video file")
                    null
                }
                print("Got file ${videoFile?.absoluteFile}");
                // Continue only if the File was successfully created
                videoFile?.also {
                    val videoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.example.android.fileprovider",
                        it
                    )
                    println("passing in video uri")
                    takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI)
                    startActivityForResult(takeVideoIntent, intent_type)
                }
            }
        }
    }

    private fun dispatchStartVideo30Event(){
        this.dispatchStartVideoDuration(30)
    }


    private fun dispatchStartStaticVideo() {
        // an open ended video that is stored as the static file
        dispatchStartVideoDuration(intent_type = REQUEST_VIDEO_STATIC)
    }

    private fun dispatchStartVideoEvent() {
        // deprecated
        // Obtain MotionEvent object
        // List of meta states found here:     developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        val metaState = 0;
        val view = findViewById<View>(android.R.id.content)

        val motionEvent = MotionEvent.obtain(
            SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
            MotionEvent.ACTION_DOWN, 0f, 0f, metaState);
        // Dispatch touch event to view
        view.dispatchTouchEvent(motionEvent);

        val motionEvent2 = MotionEvent.obtain(
            SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
            MotionEvent.ACTION_UP, 0f, 0f, metaState);
        // Dispatch touch event to view
        view.dispatchTouchEvent(motionEvent2);
    }

    private fun getMostRecentImage() : File? {
        // Find the last picture
        val projection = arrayOf(
            MediaStore.Images.ImageColumns._ID,
            MediaStore.Images.ImageColumns.DATA,
            MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
            MediaStore.Images.ImageColumns.DATE_TAKEN,
            MediaStore.Images.ImageColumns.MIME_TYPE
        )

        val cursor = MergeCursor(
            arrayOf(
                getApplicationContext().getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                    null, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC"
                ),
                getApplicationContext().getContentResolver().query(
                    MediaStore.Images.Media.INTERNAL_CONTENT_URI, projection,
                    null, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC"
                ))
        )

        if (cursor.moveToFirst()) {
            val f = File(cursor.getString(1));
            return f
        }
        return null
    }

    private fun getMostRecentVideo() : File? {
        // Find the last picture
        val projection = arrayOf(
            MediaStore.Video.VideoColumns._ID,
            MediaStore.Video.VideoColumns.DATA,
            MediaStore.Video.VideoColumns.BUCKET_DISPLAY_NAME,
            MediaStore.Video.VideoColumns.DATE_TAKEN,
            MediaStore.Video.VideoColumns.MIME_TYPE
        )

        val cursor = MergeCursor(
            arrayOf(
                getApplicationContext().getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection,
                null, null, MediaStore.Video.VideoColumns.DATE_TAKEN + " DESC"
            ),
                getApplicationContext().getContentResolver().query(
                MediaStore.Video.Media.INTERNAL_CONTENT_URI, projection,
                null, null, MediaStore.Video.VideoColumns.DATE_TAKEN + " DESC"
            ))
        )

        if (cursor.moveToFirst()) {
            val f = File(cursor.getString(1));
            return f
        }
        return null
    }

    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            sendBroadcast(mediaScanIntent)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        println("result from activity")
        // this.voltf_status = VOLTF_WAITING
        this.resumePolling()


        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == Activity.RESULT_OK) {
            var f = data?.toURI()
            println("video file ${f}")
            Toast.makeText(this, "Video file saved at ${f} or ${this.currentVideoPath}", Toast.LENGTH_LONG).show()
        }

        if (requestCode == REQUEST_VIDEO_STATIC && resultCode == Activity.RESULT_OK) {
            var f = data?.toURI()
            println("video file ${f}")
            Toast.makeText(this, "Static Video file saved at ${f} or ${this.currentStaticPath}", Toast.LENGTH_LONG).show()
        }


        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            var f = data?.toURI()
            println("image file ${f}")
            Toast.makeText(this, "Image file saved at ${f} or ${this.currentPhotoPath}", Toast.LENGTH_LONG).show()
            this.galleryAddPic()
            try {
                var f1 = File(this.currentPhotoPath)
                var f2 = File("/storage/self/primary/Movies", "image.jpg")
                f1.copyTo(f2, overwrite = true)
                println("updated primary image")
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        this.updateStats()
    }
}


