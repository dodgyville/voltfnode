# GPL3 code
# Copyright 2019 Luke Miller

from datetime import datetime
import glob
import os
from pathlib import Path

__version__ = "0.0.2"
from enum import Enum
import random
import shutil
import socket
from typing import Optional

from comparephotos import PhotoSet
from flask import Flask, render_template
from flask_restful import Api, Resource, reqparse
from flask import Flask, request
from flask import redirect, url_for
from flask_humanize import Humanize
from PIL import Image as PILImage
from pyvoltflib import VOLTF
from pyvoltflib.utils import (
    create_images_from_videos_new,
    create_images_from_videos,
    generate_voltf_from_project_directory,
    check_image_ratio,
    generate_voltf_from_image_directory)
from voltf2psx import voltf2psx

VOLTF_WAITING = 0
VOLTF_CAMERA_ON = 1
VOLTF_CAMERA_OFF = 2
VOLTF_CAMERA_START = 3
VOLTF_CAMERA_STOP = 3
VOLTF_CAMERA_IMAGE = 4  # take image
VOLTF_ASK_FOR_NAME = 5  # tell the phone to start asking for a name
VOLTF_REQUEST_NAME = 6 # request name
VOLTF_UPLOAD_IMAGE = 7
VOLTF_UPLOAD_VIDEO = 8
VOLTF_REREGISTER = 9
VOLTF_STOPPOLLING = 10
VOLTF_VIDEOINTENT = 11
VOLTF_CAMERAINTENT = 12
VOLTF_VIDEOINTENT_START = 13
VOLTF_VIDEOINTENT30 = 14
VOLTF_SELFIE = 15
VOLTF_VIDEOINTENTDURATION = 16
VOLTF_UPLOAD_STATIC = 17
VOLTF_HELLO = 18  # respond to camera ping

VOLTF_STATES = {
    None: "none",
    VOLTF_WAITING: "waiting",
    VOLTF_CAMERA_ON: "camera on",
    VOLTF_CAMERA_OFF: "camera off",
    VOLTF_CAMERA_START: "recording start",
    VOLTF_CAMERA_STOP: "recording stop",
    VOLTF_UPLOAD_IMAGE: "image upload",
    VOLTF_UPLOAD_VIDEO: "video upload",
    VOLTF_REREGISTER: "reregister node",
    VOLTF_STOPPOLLING: "stop polling",
    VOLTF_VIDEOINTENT: "open video recorder",
    VOLTF_CAMERAINTENT: "open camera app",
    VOLTF_VIDEOINTENT_START: "send stop video",
    VOLTF_VIDEOINTENT30: "30 second record",
    VOLTF_SELFIE: "take selfie",
    VOLTF_VIDEOINTENTDURATION: "variable record",
    VOLTF_UPLOAD_STATIC: "upload static video"
}


class CameraRequest(Enum):
    HELLO = "hello"


class CameraResponse(Enum):
    HELLO = VOLTF_HELLO


app = Flask(__name__, static_url_path='/takes', static_folder="takes")
humanize = Humanize(app)
api = Api(app)


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


server_ip = get_ip()
print("LOCAL IP:", server_ip)

bad_message = None  # message to display on load
good_message = None
# camera_id = 0
# voltf_status = VOLTF_WAITING
voltf_statuses = {}
server_url = f"http://{server_ip}:5000"
voltf: Optional[VOLTF] = None
start_frame = 0
end_frame = 0
duration = 30 # how many seconds to record

physicalCameras = {}
current_take_override = None

takes_dir = "takes"
snapshots_dir = Path(takes_dir) / "snapshots"
snapshots_images_dir = Path(snapshots_dir) / "images"
snapshots_track_dir = Path(takes_dir) / "snapshots_track"

for d in [takes_dir, snapshots_dir, snapshots_images_dir, snapshots_track_dir]:
    if not Path(d).exists():
        Path(d).mkdir()


class Take:
    def __init__(self, path):
        self.path = path
        self.name = Path(path).name


def get_takes():
    takes = glob.glob("takes/*")
    takes = list(sorted([x for x in takes if not Path(x).is_file() and "snapshot" not in x]))
    takes = [Take(x) for x in takes]
    return takes


takes = get_takes()


if len(takes) > 0:  # use last take as current take
    take = takes[-1].name
    try:
        current_take = datetime.strptime(take, "%Y%m%d_%H%M%S")
        print("Using last take", take)
    except ValueError:
        current_take_override = current_take = takes[-1].name
        print("using custom name?", current_take_override)
else:
    current_take = datetime.now()


def get_current_take_neat():
    global current_take_override
    if current_take_override:
        return current_take_override
    return current_take.strftime("%Y%m%d_%H%M%S")


def get_take_directory(subdirectory=None):
    take = get_current_take_neat()

    takes_directory = Path("takes")

    if not takes_directory.exists():
        takes_directory.mkdir()

    take_directory = takes_directory / take
    if not take_directory.exists():
        take_directory.mkdir()

    if subdirectory:
        sub_directory = take_directory / subdirectory
        if not sub_directory.exists():
            sub_directory.mkdir()
        return sub_directory
    return take_directory


def get_take_voltf(tmp=False):
    vdir = get_take_directory()
    tname = get_current_take_neat()
    if tmp:
        tname += "_tmp"
    return vdir / f"{tname}.voltf"


def load_voltf():
    global voltf
    if Path(get_take_voltf()).is_file():
        voltf = VOLTF().load(get_take_voltf())
        print("loaded voltf")
    else:
        print("No voltf for this take")
        voltf = None
    return voltf

voltf = load_voltf()


ROTATE0 = 0
ROTATE90 = 90
ROTATE180 = 180
ROTATE270 = 270

class RigCamera:
    def __init__(self, id=0):
        self.id = id
        self.videos_on_device = []
        self.name = None
        self.last_seen = None
        self.last_status = None
        self.aligned = -1  # -1 unknown | 0 fail | 1 weak | 2 good
        self.aligned_points = -1 # unknown
        self.voltf_status = VOLTF_WAITING
        self.uploading = False
        self.rotation = ROTATE0
        self.current_rotation = ROTATE0
        self.order = 100  # deprecating in favour of position
        self.position = 0
        self.snapshot_file = None
        self.internal = 0
        self.external = 0
        self.version = 0

    @property
    def status(self):
        if self.last_status in VOLTF_STATES:
            return VOLTF_STATES[self.last_status]
        else:
            return f"unknown ({self.last_status})"

    @property
    def alignment(self):
        if self.aligned == -1:
            return "unknown"
        elif self.aligned == 0:
            return "failed"
        elif self.aligned == 1:
            return "weak"
        elif self.aligned == 2:
            return "good"

    @property
    def thumbnail(self):
        if not self.snapshot_file:
            return f"takes/snapshots/images/camera{self.id:03}.jpg"
        else:
            return self.snapshot_file


def get_camera_by_id(id):
    cameras = [x for x in physicalCameras.values() if x.id == int(id)]
    if len(cameras) == 0:
        return None
    return cameras[0]


def generate_voltftake():
    # read the current take directory and generate the voltf file.
    vdir = get_take_directory()
    tname = get_current_take_neat()
    global voltf
    global good_message
    voltf = generate_voltf_from_project_directory(vdir)
    for video in voltf.videos:
        camera = voltf.physicalCameras[video.physicalCamera]
        camera_id = int(video.name.strip("track"))
        rig_camera = get_camera_by_id(camera_id)
        if rig_camera:
            camera.description = rig_camera.name
        else:
            camera.description = f"Camera {camera_id}"
    voltf.save(get_take_voltf())
    good_message = f"Created voltf from {len(voltf.videos)} videos"


def align_framerates_for_all_videos(edit_voltf):
    fps = [x.fps for x in edit_voltf.videos if x.fps]
    if len(set(fps)) != 1:
        print(f"Videos have differing fps values {fps}. Creating new videos to match lowest fps.")
    else:
        print(f"Videos all have same fps. That's good. Exiting.")
    new_fps = min(fps)
    new_videos = []
    for i, video in enumerate(edit_voltf.videos):
        if video.fps != new_fps:
            old_video = "takes" / Path(video.path)
            new_video = Path(str(old_video.with_suffix("")) + f"_{new_fps}fps" + old_video.suffix)
            new_videos.append(new_video)
            print(f"Will convert {old_video} to {new_video}")
            os.system(f"ffmpeg -i {old_video} -r {new_fps} -y {new_video}")
            edit_voltf.videos[i].path = str(Path(video.path).with_suffix("")) + f"_{new_fps}fps" + old_video.suffix
            edit_voltf.videos[i].fps = new_fps
    print("Finished conversion.")
    global good_message
    edit_voltf.save(edit_voltf.last_save_path)
    good_message = f"Aligned {len(new_videos)} videos and updated voltf."


def generate_psx_from_inpoint():
    global voltf
    global bad_message
    global good_message
    if not voltf:
        bad_message = "No voltf to parse."
        return
    for video in voltf.videos:
        if video.in_point:
            millis = int(video.in_point)
            ms = (millis % 1000)
            seconds = (millis / 1000) % 60
            seconds = int(seconds)
            minutes = (millis / (1000 * 60)) % 60
            minutes = int(minutes)
            hours = int((millis / (1000 * 60 * 60)) % 24)
            timestamp = f"{hours:02}:{minutes:02}:{seconds:02}.{ms:03}"
            opath = Path("takes/snapshots_track") / Path(video.path).name
            vpath = Path("takes") / video.path
            print("TIMESTAMP",video.name, timestamp, opath)
            os.system(f"ffmpeg -y -ss {timestamp} -i {vpath} -vframes 1 -q:v 2 {opath}.jpg")
        else:
            print("no in point for",video.name  )
    good_message = "Updated voltf and generated images to snapshots_track"


def generate_psx_from_snapshots():
    project_directory = Path("takes/snapshots/")
    psx_voltf = generate_voltf_from_image_directory(project_directory)
    psx_voltf.save(project_directory / "psx.voltf")

    psx_directory = Path("psx_projects")
    if not psx_directory.exists():
        psx_directory.mkdir()

    destination = psx_directory / "snapshots"
    if not destination.exists():
        destination.mkdir()

    print("exporting psx",destination)

    psx = voltf2psx(psx_voltf, output=destination, photos=True)
    psx.save(override=True)

    global good_message
    good_message = f"export PSX {destination} for snapshots ready for load into metashape"
    print("exported psx")


def generate_voltfimages():
    # voltf = VOLTF().load("/home/luke/Projects/39/tools/shared/vidcaptureHD.voltf")
    project_directory = "takes"
    duration = request.args.get("trackDuration")
    global bad_message
    global good_message
    if len(duration) > 0:
        duration = int(float(duration)*1000)
        update_voltf = create_images_from_videos_new(voltf, project_directory=project_directory, override=False, use_video_name=False, duration=duration)
        if update_voltf:
            update_voltf.save(update_voltf.last_save_path)
            good_message = "create_images_from_videos finished"
            global end_frame
            end_frame = len(update_voltf.images)/len(update_voltf.videos)
        else:
            bad_message = "create_images_from_videos failed"
    else:
        good_message = "No duration specified, cancelling."


def generate_psx():
    #destination = "/home/luke/Projects/39/tools/voltf2psx/examples"

    project_directory = get_take_directory()
    psx_voltf = generate_voltf_from_project_directory(project_directory)
    psx_voltf.save(project_directory / "psx.voltf")

    psx_directory = Path("psx_projects")
    if not psx_directory.exists():
        psx_directory.mkdir()

    destination = psx_directory / get_current_take_neat()
    if not destination.exists():
        destination.mkdir()

    print("exporting psx",destination)

    psx = voltf2psx(psx_voltf, output=destination, photos=True,
            use_camera_names=True, frames=psx_voltf.scenes[0].frames)
    psx.save(override=True)

    global good_message
    good_message = f"export PSX {destination} ready for load into metashape"
    print("exported psx")


def update_voltf_from_request(edit_voltf, request):
    """ take the form data and update our voltf """
    # in points:
    for video_index, x in enumerate(request.args.getlist("in_point[]")):
        x = None if x.upper() == 'NONE' else int(x)
        edit_voltf.videos[video_index].in_point = x

    for video_index, x in enumerate(request.args.getlist("out_point[]")):
        x = None if x.upper() == 'NONE' else int(x)
        edit_voltf.videos[video_index].out_point = x
    global bad_message
    global good_message
    if not edit_voltf:
        bad_message = "No voltf to update"
        return

    if edit_voltf.last_save_path:
        edit_voltf.save(edit_voltf.last_save_path)
    else:
        v = get_take_voltf(tmp=True)
        print(f"saving to {v}")
        edit_voltf.save(v)
    good_message = "voltf updated"

def PILrotate(degrees):
    # PIL is counter clockwise, we are clockwise
    if degrees == 0:
        return 0
    elif degrees == 90:
        return PILImage.ROTATE_270
    elif degrees == 180:
        return PILImage.ROTATE_180
    elif degrees == 270:
        return PILImage.ROTATE_90


def check_image_rotation(camera, fname):
    p = Path(fname)
    if p.is_file():
        try:
            im = PILImage.open(fname)
            im = im.transpose(PILrotate((-camera.current_rotation)%360))
            im = im.transpose(PILrotate(camera.rotation))

            camera.current_rotation = camera.rotation
            im.save(fname)
        except:
            pass


def save_snapshot(request, camera):
    # make sure file is in 1920x1080
    f = request.files[[x for x in request.files.keys()][0]]
    if "tmp" in f.filename:
        print(f"Photo is empty for {camera.id}")
        return
    try:
        f.save(camera.thumbnail)
        # check_image_rotation(camera, camera.thumbnail)
        check_image_ratio(camera.thumbnail)
    except OSError:
        global bad_message
        bad_message = f"Unable to save snapshot for {camera.id}"
        camera.voltf_status = VOLTF_WAITING


def copyDirectory(src, dest):
    try:
        shutil.copytree(src, dest)
    # Directories are the same
    except shutil.Error as e:
        print('Directory not copied. Error: %s' % e)
    # Any error saying that the directory doesn't exist
    except OSError as e:
        print('Directory not copied. Error: %s' % e)


class Cameras(Resource):
    def get(self, command):
        global physicalCameras
        global good_message
        global bad_message
        global current_take_override
        global duration

        bad_message = None
        good_message = None

        # these come from the web interface and are orders to the nodes
        cameras = request.args.getlist('camera')
        camera_ids = [int(x) for x in cameras]
        requestedCameras = [x for x in physicalCameras.values() if x.id in camera_ids]

        orders = request.args.getlist("order[]")
        cs = list(physicalCameras.values())
        for i, order in enumerate(orders):
            cs[i].order = int(order)

        # global voltf_status
        print('command', command, 'for cameras', cameras)
        voltf_status_new = None
        if command == "on":
            print("turning on cameras")
            voltf_status_new = VOLTF_CAMERA_ON
        elif command == "off":
            print("turning off cameras")
            voltf_status_new = VOLTF_CAMERA_OFF
        elif command == "rotate90":
            for c in requestedCameras:
                c.rotation = (c.rotation + 90) % 360
                check_image_rotation(c, c.thumbnail)
        elif command == "rotate90":
            for c in requestedCameras:
                c.rotation = (c.rotation - 90) % 360
                check_image_rotation(c, c.thumbnail)
        elif command == "rotate180":
            for c in requestedCameras:
                c.rotation = (c.rotation + 180) % 360
                check_image_rotation(c, c.thumbnail)
        elif command == "photos":
            print("taking photos with cameras")
            voltf_status_new = VOLTF_CAMERA_IMAGE
        elif command == "uploadshot":
            voltf_status_new = VOLTF_UPLOAD_IMAGE
        elif command == "storesnapshot":
            project_directory = Path("takes/snapshots/")
            newname = request.args["snapshotname"]
            copyDirectory("takes/snapshots", f"snapshots/{newname}")
        elif command == "selfie":
            voltf_status_new = VOLTF_SELFIE
        elif command == "snapshot":
            print("taking photos with cameras")
            voltf_status_new = VOLTF_CAMERA_IMAGE
        elif command == "unregister":
            cameras = request.args.getlist('camera')
            camera_ids = [int(x) for x in cameras]
            unregister_cameras = [x for x in physicalCameras.values() if x.id in camera_ids]
            for c in unregister_cameras:
                print("unregistering",c.name, c.id)
                del physicalCameras[c.name]
            voltf_status_new = VOLTF_WAITING
        elif command == "videointent":
            if len(request.args["duration"]) > 0:
                duration = int(request.args["duration"])
                voltf_status_new = VOLTF_VIDEOINTENTDURATION
        elif command == "videointentstart":
            voltf_status_new = VOLTF_VIDEOINTENT_START
        elif command == "videointent30":
            voltf_status_new = VOLTF_VIDEOINTENT30
        elif command == "cameraintent":
            voltf_status_new = VOLTF_CAMERAINTENT
        elif command == "start":
            voltf_status_new = VOLTF_CAMERA_START
        elif command == "stop":
            voltf_status_new = VOLTF_CAMERA_STOP
        elif command == "uploadvideo":
            voltf_status_new = VOLTF_UPLOAD_VIDEO
        elif command == "uploadstatic":
            voltf_status_new = VOLTF_UPLOAD_VIDEO
        elif command == "newtake":
            global current_take
            global current_take_override
            current_take_override = None
            current_take = datetime.now()
            good_message = "New take"
        elif command == "stoppolling":
            voltf_status_new = VOLTF_STOPPOLLING
        elif command == "voltftake": # generate a take from the raw video files
            # voltf_status_new = VOLTF_CAMERAINTENT
            generate_voltftake()
        elif command == "renametake":
            newname = request.args["takename"]
            if len(newname.strip()) == 0:
                return

            path = get_take_directory()
            newpath = path.parent / newname

            path.rename(newname)

            import pdb; pdb.set_trace()

            current_take_override = newname

            # rename_take(request.args)
        elif command == "settake":
            takename = request.args["settake"]
            takes = get_takes()
            found = [x for x in takes if x.name == takename]
            if len(found) == 0:
                bad_message = "No take found with that name"
            elif len(found) > 1:
                bad_message = "Multiple takes found with that name"
            else:
                current_take_override = found[0].name
                load_voltf()
        elif command == "generateimages": # generate a take from the raw video files
            generate_voltfimages()
        elif command == "alignfps":
            align_framerates_for_all_videos(voltf)
        elif command == "videosnapshotpsx":
            generate_psx_from_inpoint()
        elif command == "psxvoltf": # generate a psx from the current take voltf
            generate_psx()
        elif command == "savevoltf":
            update_voltf_from_request(voltf, request)
        elif command == "psxsnapshots":
            generate_psx_from_snapshots()
        elif command == "alignment":
            photoset = PhotoSet()
#            photoset.import_directory("takes/snapshots/images/", "*.jpg")
            flist = [x.thumbnail for x in requestedCameras]
            print("Will SIMPLE compare",flist)
            photoset.import_files(flist)
            photoset.simple_pairing()
            photoset.compare_photos()
            photoset.save("takes/snapshots/snapshots.voltf")
            for comparison in photoset.comparisons:
                name = photoset.photos[comparison.photoLeft].name
                camera_id = None
                if name.startswith("camera"):
                    name = name.split("camera")[1]
                    camera_id = int(name)
                camera = get_camera_by_id(camera_id) if camera_id else None
                if not camera:
                    print(f"Unable to find camera {comparison.photoLeft}")
                    continue
                i = camera.aligned_points = len(comparison.pointsLeft)
                if i <= 30:
                    camera.aligned = 0  # fail
                elif i <= 100:
                    camera.aligned = 1  # weak
                else:
                    camera.aligned = 2  # strong
            good_message = "Finished alignment"
        elif command == "waiting":
            voltf_status_new = VOLTF_WAITING
        elif command == "fake": # generate cameras from existing snapshots

            fakes_dir = Path(takes_dir) / "fakes/images"
            files = Path(fakes_dir).glob("*.jpg")
            files = sorted(files)

            for i, f in enumerate(files):
                print(f)
                c = RigCamera()
                camera_id = Path(f).stem
                c.name = camera_id
                c.id = int(camera_id.split("camera")[1])
                c.last_seen = datetime.now()
                c.snapshot_file = f
                physicalCameras[c.name] = c
        else:
            print("UNKNOWN GET COMMAND",command)

        if voltf_status_new:
            for c in requestedCameras:
                c.voltf_status = voltf_status_new
                if voltf_status_new in [VOLTF_UPLOAD_VIDEO, VOLTF_UPLOAD_IMAGE]:
                    c.uploading = True
        return redirect(url_for('homepage'))

    def post(self, command, *args, **kwargs):
        # these come from the android app on the nodes
        global physicalCameras
        print("basic post from app with command:",command)
        cameras = request.args.getlist('camera')
        print("affects cameras",cameras)
        if command == "register":
            print("hello new camera!")
            name = request.values["name"]
            camera_id = request.values["id"]
            if len(camera_id) == 0:
                print("Can't get camera_id!")
                camera_id = random.randint(100,999)
            camera_id = int(camera_id)
            print(name, camera_id)
            #global camera_id
            #camera_id += 1
            c = RigCamera()
            c.id = camera_id
            c.last_seen = datetime.now()
            c.name = name
            if name in physicalCameras:
                print("**** REREGISTER", name,"****")
            physicalCameras[name] = c
            print("register new camera", camera_id)
            save_snapshot(request, c)
            # request.files["image"].save(f"images/{camera_id}.jpg")
            print("saved image")
            print(f"Registered phone {c.id}, so turn it into waiting mode")
            c.voltf_status = VOLTF_WAITING
            return {"id": camera_id, "duration": duration, "voltf_status": c.voltf_status}
        else:
            print("UNKNOWN POST COMMAND",command)


class Camera(Resource):
    def get(self, camera_id, command):
        # basic polling api point for all nodes
        camera = get_camera_by_id(camera_id)
        internal = request.args.get("internal")
        external = request.args.get("external")
        version = request.args.get("version")

        if not camera:
            print(f"Force a reregister because {camera_id} has polled but not been found.")
            return {"id": -1, "duration": duration, "voltf_status": VOLTF_REREGISTER}

        for stat in ["internal", "external", "version"]:
            s = request.args.get(stat)
            if s:
                setattr(camera, stat, s)

        print("camera",camera_id,"wants instructions")
        if command == "instruction":
            return {"id": camera.id, "duration": duration, "voltf_status": camera.voltf_status}

    def post(self, camera_id, command):
        # when a camera responds to an instruction with an upload
        camera = [x for x in physicalCameras.values() if x.id == int(camera_id)][0]
        camera.last_status = command
        camera.last_seen = datetime.now()
        print("post from app",camera_id, command)
        if command == "uploadshot":
            print("received screenshot from ",camera_id)
            save_snapshot(request, camera)
            camera.uploading = False
            camera.voltf_status = VOLTF_WAITING
        elif command == "uploadvideo":
            print("receiving video from ",camera_id)
            d = get_current_take_neat()
            track_name = f"track{int(camera_id):03}"
            vdir = get_take_directory(track_name)
            vname = f"{vdir}/{track_name}.mp4"  # takes/DATE/DATE.mp4
            request.files[[x for x in request.files.keys()][0]].save(vname)
            print("finished receiving video from ",camera_id)
            camera.uploading = False
            camera.voltf_status = VOLTF_WAITING
        elif command == "uploadstatic":
            print("receiving static video from ",camera_id)
            d = get_current_take_neat()
            track_name = f"track{int(camera_id):03}"
            vdir = get_take_directory(track_name)
            vname = f"{vdir}/static.mp4"  # takes/DATE/DATE.mp4
            request.files[[x for x in request.files.keys()][0]].save(vname)
            print("finished receiving static video from ",camera_id)
            camera.uploading = False
            camera.voltf_status = VOLTF_WAITING
        else:
            print("UNKNOWN POST2 COMMAND",command)
        return {"id": camera.id, "duration": duration, "voltf_status": camera.voltf_status}


@app.route("/")
def homepage():
    cameras = list(physicalCameras.values())
    cameras = sorted(cameras, key=lambda x: x.id)
    aligned = [x for x in cameras if x.aligned >= 1]
    print("cameras",cameras)
    ordered = sorted(cameras, key=lambda x: x.order, reverse=False)
    current_time_neat = datetime.now().strftime("%Y%m%d_%H%M%S")
    takes = get_takes()
    print(takes)
    return render_template('index.html', duration=duration,
                           bad_message=bad_message, good_message=good_message,
                           cameras=cameras, aligned=aligned,
                           ordered=ordered,
                           server_url=server_url, now=datetime.now(),
                           current_take_neat=get_current_take_neat(),
                           current_time_neat=current_time_neat,
                           available_takes=takes,
                           voltf=voltf)


@app.route("/hello/")
def helloworld():
    return {
        "name": "VoltfNodeServer",
        "version": __version__,
        "available": [],  # which services are available (eg compare photos)
    }


api.add_resource(Cameras, "/cameras/<string:command>/")
api.add_resource(Camera, "/camera/<string:camera_id>/<string:command>/")

print("cleaning up images directory")
for f in Path("takes/snapshots/images/").glob("*.jpg"):
    f.unlink()

app.run(host='0.0.0.0', debug=True)
